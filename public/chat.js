const socket = io.connect()

var chat = document.getElementById('chat')
var nome = document.getElementById('nome')
var mensagem = document.getElementById('mensagem')
var btn = document.getElementById('botao')
var online = document.getElementById('id')
var pessoas_on = document.getElementById('pessoas_on')
var output = document.getElementById('output')



mensagem.addEventListener('keypress', (e) =>{
    let msg = nome.value +' está digitando...'
    socket.emit('escrevendo', msg)
})

btn.addEventListener('click', (e) => {
    e.preventDefault()
    socket.emit('mensagem', {
        name: nome.value,
        message: mensagem.value
    })
    socket.emit('escrevendo', '')
    mensagem.value = ''
})

socket.on('mensagem', (data) => {
    chat.innerHTML += '<p id="pMsg"><b>'+ data.name + '</b>: ' + data.message + '</p>'
})
socket.on('new user', (userOnline) => {
    pessoas_on.innerHTML = 'Usuarios online: '+ userOnline
})

socket.on('idOnline', (users) => {
    online.innerHTML = ''
    for (us of users) {
        online.innerHTML += '<p>'+ us.id +'</p>'  
    }
})

socket.on('escrevendo', (nome) => {
     output.innerHTML = nome
})



