const express = require('express')
const app = express()
const socket = require('socket.io')
const server = app.listen(8081, () => {
    console.log('ouvindo 8081')
})
const io = socket(server)


app.use(express.static('public'))

app.get('/', (req, res) => {
    res.sendFile(__dirname + 'index.html')
})

var usuarios = [{id: ''}]

io.on('connection', (socket) => {
    console.log('novo usuario: '+ socket.id)

    io.emit('new user', usuarios.length)

    usuarios.push({id: socket.id})

    
    io.emit('idOnline', usuarios)

    
    socket.on('disconnect', () => {
        console.log('usuario desconectado: '+ socket.id)
        io.emit('escrevendo', '')
        
        for (let i = 0; i < usuarios.length; ++i) {
            if(usuarios[i].id == socket.id){
                usuarios.splice(i, 1)
                break;
            }
        }

        io.emit('idOnline', usuarios)
        
        io.emit('new user', usuarios.length - 1)
    })
    socket.on('mensagem', (data) => {
        console.log(data)
        io.emit('mensagem', data)
    })
    socket.on('escrevendo', (nome) => {
        socket.broadcast.emit('escrevendo', nome)
    })
    
})


